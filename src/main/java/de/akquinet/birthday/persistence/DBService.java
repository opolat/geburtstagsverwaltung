package de.akquinet.birthday.persistence;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import de.akquinet.birthday.model.Employee;

/**
 * Datenbank Zugriffsklasse mit CRUD-Operationen.
 * 
 */
public class DBService {

	private static EntityManager entityManager;
	private static EntityManagerFactory factoryTmt;

	/**
	 * Closes the entity manager factories.
	 */
	public static void closeEntityManagerFactory() {
		factoryTmt.close();
	}

	/**
	 * Returns the {@link EntityManager} of the TMT persistence.
	 * 
	 * @return the {@link EntityManager} of the TMT database.
	 */
	private static EntityManager getEntityManager() {

		if (entityManager == null) {
			try {
				openEntityManagerFactory();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return entityManager;
	}

	/**
	 * Opens the entity manager factories.
	 * 
	 * @throws IOException
	 *             in case of config or connection errors
	 */
	public static void openEntityManagerFactory() throws IOException {

		factoryTmt = Persistence.createEntityManagerFactory("mitarbeiter");
		entityManager = factoryTmt.createEntityManager();

	}

	/**
	 * Speichert einen Mitarbeiter
	 * 
	 * @param employee
	 */
	public void save(Employee employee) {

		EntityManager entityManager = getEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		if (!entityTransaction.isActive()) {
			entityTransaction.begin();
		}
		
		// TODO: DB
		// Mitarbeiter speichern
		
		entityTransaction.commit();

	}

	/**
	 * �ndert einen Mitarbeiter
	 * 
	 * @param employee
	 */
	public void delete(Employee employee) {


		EntityManager entityManager = getEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();
		
		// TODO: DB
		// Mitarbeiter loeschen


		entityTransaction.commit();
	}

	/**
	 * Aktualisiert einen Mitarbeiter
	 * 
	 * @param employee
	 */
	public void update(Employee employee) {

		EntityManager entityManager = getEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();

		entityTransaction.begin();
		
		// TODO: DB
		// Mitarbeiter loeschen
		
		entityTransaction.commit();

	}

	public List<Employee> getAllEmployees() {
		
		// TODO: DB
		// Alle Mitarbeiter zurueckgeben


		return null;
	}
}
