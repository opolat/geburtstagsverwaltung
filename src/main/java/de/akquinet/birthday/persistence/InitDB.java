package de.akquinet.birthday.persistence;

import java.io.IOException;

import de.akquinet.birthday.model.Employee;

public class InitDB {

	/**
	 * Initialisiert die Datenbank mit Daten
	 * 
	 * @throws IOException
	 */
	static public void init() throws IOException {
		DBService dbService = new DBService();

		dbService.openEntityManagerFactory();
		
		//TODO: lege hier zwei Mitarbeiter Datensätze in der Datenbank an.

		dbService.closeEntityManagerFactory();
	}

}
