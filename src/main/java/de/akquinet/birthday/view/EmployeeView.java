package de.akquinet.birthday.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableColumn;

import de.akquinet.birthday.model.EmployeeTableModel;

/**
 * Das Hauptfenster der Anwendung
 *
 */
public class EmployeeView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String EMPLYOYEE_ID = "Nr";
	private static final String EMPLYOYEE_NAME = "Name";
	private static final String EMPLYOYEE_FIRSTNAME = "Vorname";
	private static final String EMPLYOYEE_BIRTHDAY = "Geburtsdatum";

	private JPanel contentPane;

	private JTextField textFieldName;
	private JTextField textFieldFirstName;
	private JTextField textFieldBirthday;

	private JButton btnAdd;
	private JButton btnDelete;
	private JButton btnNew;

	private JTable table;
	private JButton btnSave;

	/**
	 * Hier wird die Oberfläche gebaut.
	 */
	public EmployeeView() {
		setTitle("Geburtstagslisten-Verwaltung");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 623, 419);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0 };
		gbl_panel.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
		gbl_panel.columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_panel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0, 1.0,
				Double.MIN_VALUE };
		panel.setLayout(gbl_panel);

		JLabel lblName = new JLabel(EMPLYOYEE_NAME + ":");
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.anchor = GridBagConstraints.WEST;
		gbc_lblName.gridx = 0;
		gbc_lblName.gridy = 0;
		panel.add(lblName, gbc_lblName);

		textFieldName = new JTextField();
		textFieldName.setName("textFieldName");
		GridBagConstraints gbc_textFieldName = new GridBagConstraints();
		gbc_textFieldName.gridwidth = 3;
		gbc_textFieldName.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldName.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldName.gridx = 1;
		gbc_textFieldName.gridy = 0;
		panel.add(textFieldName, gbc_textFieldName);
		textFieldName.setColumns(10);

		btnNew = new JButton("Neu");
		GridBagConstraints gbc_btnNeu = new GridBagConstraints();
		gbc_btnNeu.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnNeu.insets = new Insets(0, 0, 5, 0);
		gbc_btnNeu.gridx = 12;
		gbc_btnNeu.gridy = 0;
		panel.add(btnNew, gbc_btnNeu);

		JLabel lblVorname = new JLabel(EMPLYOYEE_FIRSTNAME + ":");
		GridBagConstraints gbc_lblVorname = new GridBagConstraints();
		gbc_lblVorname.anchor = GridBagConstraints.WEST;
		gbc_lblVorname.insets = new Insets(0, 0, 5, 5);
		gbc_lblVorname.gridx = 0;
		gbc_lblVorname.gridy = 1;
		panel.add(lblVorname, gbc_lblVorname);

		textFieldFirstName = new JTextField();
		GridBagConstraints gbc_textFieldVorname = new GridBagConstraints();
		gbc_textFieldVorname.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldVorname.gridwidth = 3;
		gbc_textFieldVorname.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldVorname.gridx = 1;
		gbc_textFieldVorname.gridy = 1;
		panel.add(textFieldFirstName, gbc_textFieldVorname);
		textFieldFirstName.setColumns(10);

		btnAdd = new JButton("Hinzuf\u00FCgen");
		btnAdd.setName("btnHinzufuegen");
		GridBagConstraints gbc_btnHinzufuegen = new GridBagConstraints();
		gbc_btnHinzufuegen.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnHinzufuegen.insets = new Insets(0, 0, 5, 0);
		gbc_btnHinzufuegen.gridx = 12;
		gbc_btnHinzufuegen.gridy = 1;
		panel.add(btnAdd, gbc_btnHinzufuegen);

		JLabel lblGeburtsdatum = new JLabel(EMPLYOYEE_BIRTHDAY + ":");
		GridBagConstraints gbc_lblGeburtsdatum = new GridBagConstraints();
		gbc_lblGeburtsdatum.anchor = GridBagConstraints.WEST;
		gbc_lblGeburtsdatum.insets = new Insets(0, 0, 5, 5);
		gbc_lblGeburtsdatum.gridx = 0;
		gbc_lblGeburtsdatum.gridy = 2;
		panel.add(lblGeburtsdatum, gbc_lblGeburtsdatum);

		textFieldBirthday = new JTextField();
		GridBagConstraints gbc_textFieldGeburtsdatum = new GridBagConstraints();
		gbc_textFieldGeburtsdatum.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldGeburtsdatum.gridwidth = 3;
		gbc_textFieldGeburtsdatum.insets = new Insets(0, 0, 5, 5);
		gbc_textFieldGeburtsdatum.gridx = 1;
		gbc_textFieldGeburtsdatum.gridy = 2;
		panel.add(textFieldBirthday, gbc_textFieldGeburtsdatum);
		textFieldBirthday.setColumns(10);

		btnDelete = new JButton("L\u00F6schen");
		GridBagConstraints gbc_btnLoeschen = new GridBagConstraints();
		gbc_btnLoeschen.anchor = GridBagConstraints.NORTH;
		gbc_btnLoeschen.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnLoeschen.insets = new Insets(0, 0, 5, 0);
		gbc_btnLoeschen.gridx = 12;
		gbc_btnLoeschen.gridy = 2;
		panel.add(btnDelete, gbc_btnLoeschen);
		
		btnSave = new JButton("Speichern");
		GridBagConstraints gbc_btnSave = new GridBagConstraints();
		gbc_btnSave.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnSave.insets = new Insets(0, 0, 5, 0);
		gbc_btnSave.gridx = 12;
		gbc_btnSave.gridy = 3;
		panel.add(btnSave, gbc_btnSave);
		
		EmployeeTableModel model = new EmployeeTableModel();
		model.addColumn(EMPLYOYEE_ID);
		model.addColumn(EMPLYOYEE_NAME);
		model.addColumn(EMPLYOYEE_FIRSTNAME);
		model.addColumn(EMPLYOYEE_BIRTHDAY);

		table = new JTable(model);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		TableColumn columnNr = table.getColumnModel().getColumn(0);
        table.getColumnModel().removeColumn(columnNr);
		
		GridBagConstraints gbc_table_1 = new GridBagConstraints();
		gbc_table_1.gridheight = 2;
		gbc_table_1.gridwidth = 12;
		gbc_table_1.insets = new Insets(0, 0, 0, 5);
		gbc_table_1.fill = GridBagConstraints.BOTH;
		gbc_table_1.gridx = 0;
		gbc_table_1.gridy = 4;
		JScrollPane jScrollPane = new JScrollPane(table);
		panel.add(jScrollPane, gbc_table_1);

	}

	// *********** Getter/Setter Methoden **************

	public JButton getBtnSave() {
		return btnSave;
	}

	public JButton getBtnNew() {
		return btnNew;
	}

	public JButton getBtnAdd() {
		return btnAdd;
	}

	public JButton getBtnDelete() {
		return btnDelete;
	}

	public JTextField getTextFieldName() {
		return textFieldName;
	}

	public void setTextFieldName(JTextField textFieldName) {
		this.textFieldName = textFieldName;
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public JTextField getTextFieldFirstName() {
		return textFieldFirstName;
	}

	public void setTextFieldFirstName(JTextField textFieldFirstName) {
		this.textFieldFirstName = textFieldFirstName;
	}

	public JTextField getTextFieldBirthday() {
		return textFieldBirthday;
	}

	public void setTextFieldBirthday(JTextField textFieldBirthday) {
		this.textFieldBirthday = textFieldBirthday;
	}

	public void setController() {

	}

	/**
	 * Startet nur das JFrame ohne Controller.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EmployeeView frame = new EmployeeView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
