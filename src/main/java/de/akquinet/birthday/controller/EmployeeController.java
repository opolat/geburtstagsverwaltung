package de.akquinet.birthday.controller;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.List;

import javax.swing.JOptionPane;

import de.akquinet.birthday.model.Employee;
import de.akquinet.birthday.model.EmployeeTableModel;
import de.akquinet.birthday.persistence.DBService;
import de.akquinet.birthday.persistence.InitDB;
import de.akquinet.birthday.view.EmployeeView;

/**
 *
 *
 */
public class EmployeeController {

	DBService dbService = new DBService();

	/**
	 * Alle Listener die f�r die Interaktion ben�tigt werden, werden hier
	 * registriert.
	 * 
	 * @param frame
	 */
	private void registerListener(final EmployeeView frame) {
		
		/**
		 * Listener zum Hinzuf�gen eines Mitarbeiters
		 */
		frame.getBtnAdd().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				// TODO: GUI
				// Ein neuer Mitarbeiter wird in die Tabelle hinzugefügt

				// TODO: DB
				// hier die DBService-Methode zum hinzufügen eines neuen
				// Mitarbeiters in die Datenbank aufrufen

			}
		});

		/**
		 * Listener zum L�schen eines Mitarbeiters
		 */
		frame.getBtnDelete().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {


					// TODO: GUI
					// Der neu Mitarbeiter wird in der Tabelle selektiert und dann gelöscht
					// Vorher bitte abfragen, ob wirklich gelöscht werden soll

					// TODO: DB
					// hier die DBService-Methode zum löschen eines
					// Mitarbeiters aus der Datenbank aufrufen

			}
		});

		/**
		 * Listener zum aktualisieren eines Mitarbeiters
		 */
		frame.getBtnSave().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				// TODO: GUI
				// Der Mitarbeiter wird nach der Aktualisierung in die Tabelle übernommen


				// TODO: DB
				// hier die DBService-Methode zum speichern eines
				// Mitarbeiters aufrufen
				

			}
		});

		/**
		 * Leert die Eingabe-Felder f�r einen neuen Eintrag
		 */
		frame.getBtnNew().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				// TODO: GUI 
				// alle Felder löschen für Neuanlage

			}
		});

		/**
		 * Durch doppelklick in die Zeile wird die Zeile zur Bearbeitung
		 * �bernommen
		 * 
		 */
		frame.getTable().addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {

				// TODO: GUI
				// Durch doppelklick den Eintrag in die Felder übernehmen

			}

		});

	}

	void init(final EmployeeView frame) throws IOException {

		InitDB.init();

		registerListener(frame);

		// TODO: DB
		// Beim starten sollen alle Mitarbeiter aus der Datenbank in die
		// Tabelle übernommen werden
	}

	/**
	 * Startet die Anwendung.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					final EmployeeView frame = new EmployeeView();
					new EmployeeController().init(frame);

					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
