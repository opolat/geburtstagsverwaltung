package de.akquinet.birthday.model;

import javax.swing.table.DefaultTableModel;

/**
 * Das Tabellen Model, welches die Daten anzeigt
 *
 */
public class EmployeeTableModel extends DefaultTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;



	public void addRow(Employee employee) {

		String[] row = { String.valueOf(employee.getId()), employee.getName(), employee.getFirstname(),
				employee.getBirthday() };

		super.addRow(row);
	}
	
	public void updateRow(int rowIndex, Employee employee) {
		
		removeRow(rowIndex);

		String[] row = { String.valueOf(employee.getId()), employee.getName(), employee.getFirstname(),
				employee.getBirthday() };

		super.insertRow(rowIndex, row);
	}

	public Employee getRowObject(int row) {

		Employee employee = new Employee();

		
		employee.setId(Long.parseLong((String)getValueAt(row, 0)));
		employee.setName((String) getValueAt(row, 1));
		employee.setFirstname((String) getValueAt(row, 2));
		employee.setBirthday((String) getValueAt(row, 3));

		return employee;
	}
	
	
	
	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}
}
