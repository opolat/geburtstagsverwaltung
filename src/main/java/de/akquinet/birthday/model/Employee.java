package de.akquinet.birthday.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Repräsentiert die Entität Mitarbeiter
 * 
 */
@Entity
@Table(name = "Employee")
public class Employee {

	@Id
	@GeneratedValue
	@Column(name = "IDENTIFIER")
	private Long id;

	private String name;
	private String firstname;
	private String birthday;
	
	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String geburtsdatum) {
		this.birthday = geburtsdatum;
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + ", firstname=" + firstname
				+ ", birthday=" + birthday + ", id=" + id + "]";
	}

}
