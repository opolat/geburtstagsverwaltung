package de.akquinet.birthday.fitnesse;

import org.junit.Assert;

import de.akquinet.birthday.controller.EmployeeController;
import de.signaliduna.atf.adapters.fest.ApplicationStopListener;
import de.signaliduna.atf.adapters.fest.FestGuiAdapter;

public class Fixture {

	static FestGuiAdapter guiAdapter = new FestGuiAdapter();

	public boolean starteDieAnwendung() {
		
		guiAdapter.registerStopApplicationListener(new ApplicationStopListener() {
			
			@Override
			public void terminateApplication() {
				//
			}
		});

		// startet die SUT
		guiAdapter
				.startApplication(EmployeeController.class, new String[] { "" });
		Assert.assertTrue(guiAdapter.waitUntilFrameIsVisible(1,
				".*Geburtstagslisten.*"));
		
		return true;
	}
	
	public boolean beendeDieAnwendung() {

		// startet die SUT
		guiAdapter
				.stopApplication();
		
		return true;
	}

	public void click(String text) throws Exception {
		guiAdapter.clickOnButtonByText(text);
	}
	
	public void enter(String text, String locator) {
		guiAdapter.typeIntoField(text, locator);
		
	}

}
