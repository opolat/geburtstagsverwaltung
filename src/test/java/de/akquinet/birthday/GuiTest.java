package de.akquinet.birthday;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import de.akquinet.birthday.controller.EmployeeController;
import de.signaliduna.atf.adapters.fest.ApplicationStopListener;
import de.signaliduna.atf.adapters.fest.FestGuiAdapter;

public class GuiTest {

	private FestGuiAdapter guiAdapter;

	@Before
	public void setUp() {
		guiAdapter = new FestGuiAdapter();

		guiAdapter
				.registerStopApplicationListener(new ApplicationStopListener() {

					@Override
					public void terminateApplication() {
						//
					}
				});
	}

	/**
	 * Startet die Anwendung und klickt einfach auf den Hinzufügen-Button
	 * @throws Exception
	 */
	@Test
	public void startSUT() throws Exception {

		// startet die SUT
		guiAdapter
				.startApplication(EmployeeController.class, new String[] { "" });

		// wartet bis das Hauptframe sichtbar wird.
		Assert.assertTrue(guiAdapter.waitUntilFrameIsVisible(1,
				".*Geburtstagslisten.*"));

		Assert.assertEquals("Geburtstagslisten-Verwaltung", guiAdapter
				.getWindowFrame().getTitle());
		
		// Werte eingeben
		guiAdapter.typeIntoField("Mustermann", "textFieldName");
		

		guiAdapter.clickOnButtonByName("btnHinzufuegen");

	}
}
