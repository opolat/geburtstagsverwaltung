package de.akquinet.birthday;
import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;

import de.akquinet.birthday.model.Employee;
import de.akquinet.birthday.persistence.DBService;

public class DBTest {
	
	DBService dbService = new DBService();
	
	@BeforeClass
	static public void setUp() {
		try {
			DBService.openEntityManagerFactory();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Testet das Speichern eines Mitarbeiters
	 */
	@Test
	public void save() {

		// TODO: DB
		// lege einen Mitarbeiter an und pruefe
		// anschliessend, der Mitarbeiter existiert

	}

	/**
	 * Testet das Loeschen eines Mitarbeiters
	 */
	@Test
	public void delete() {

		// TODO: DB
		// lege 2 Mitarbeiter an und loesche einen davon
		// und pruefe anschliessend ob der gelöschte tatsaechlich nicht mehr existiert
		
	}


	/**
	 * Testet die Aktualisierung eines Mitarbeiters
	 */
	@Test
	public void update(Employee employee) {
		
		// TODO: DB
		// lege einen Mitarbeiter an, anschliessend änderst Du das Geburtsdatum unsd pruefst, 
		// ob dieser sich tatsaechlich veraendert hat
	}

	/**
	 * Testet die Liste von Mitarbeitern
	 */
	@Test
	public void getAllEmployees() {
		
		// TODO: DB
		// Lege 3 Mitarbeiter an und pruefe ob die Methode tatsaechlich 3 Mitarbeiter zurueckliefrt

	}
}
