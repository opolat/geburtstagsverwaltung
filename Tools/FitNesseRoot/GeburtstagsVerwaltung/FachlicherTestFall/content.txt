!*> Initialisierung

!define TEST_SYSTEM {slim}

!path c:\Studententag\fixture.jar 

|import|
|de.akquinet.birthday.fitnesse|

|Library|
|Fixture|

|scenario|Gebe den Wert _ in das Feld Name ein |wert |
|enter;|@wert|textFieldName|

|scenario|Klicke auf den Button _ | buttonText|
|click|@buttonText|

*!

!3 Beispiel für einen fachlichen Abnahmetest

-!|script|
|starte die Anwendung|
|Gebe den Wert Mustermann in das Feld Name ein|
|Klicke auf den Button Hinzufügen|
|beende die Anwendung|
